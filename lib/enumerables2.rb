require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0, :+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? {|long_str| long_str.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  only_letters = string.gsub(' ', '')
  repeats = only_letters.chars.select {|letter| string.count(letter) > 1}
  repeats.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  clean_string = string.gsub(/[?.',"]/, "")
  sorted_array = clean_string.split(" ").sort_by {|word| word.length}
  sorted_array[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z").to_a
  alphabet.reject {|ltr| string.include?(ltr)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|year| not_repeat_year?(year)}
end

def not_repeat_year?(year)
  year_digits = year.to_s.chars
  year_digits.all? {|digit| year_digits.count(digit) < 2}
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.uniq.select {|song| no_repeats?(song, songs)}
end

def no_repeats?(song_name, songs)
  (1..songs.count-1).none? {|idx| songs[idx] == song_name && songs[idx-1] == song_name}
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.gsub(/[.?!,;-]/, '').split(' ')
  words.select! {|word| word.include?("c")}
  return "" if words.count == 0
  words.reduce do |last_c, word|
    if c_distance(word) < c_distance(last_c)
      word
    else
      last_c
    end
  end
end

def c_distance(word)
  clean_word = word.downcase.reverse
  last_c = nil
  clean_word.chars.each_with_index do |letter, idx|
    if letter == "c"
      last_c = idx
      break
    end
  end
  last_c
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeats_arr = []
  x = 0
  while x < arr.length-1
    y = x + 1
    if arr[x] == arr[y]
      sub_arr = [x]
      while arr[x] == arr[y]
        y += 1
      end
      sub_arr << y-1
      repeats_arr << sub_arr
    end
    x = y
  end
  repeats_arr
end
